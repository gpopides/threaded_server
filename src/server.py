"""Server class."""
import socket
import threading
import random
import json
import time


class Server:
    def __init__(self):
        self.lock = threading.Lock()
        self.ip_dictionary = self.create_dictionary()

    def create_dictionary(self):
        ips = {
            'uom': "192.215.1.1",
            'debian': "192.120.3.5"
        }
        return ips

    def get_time_delay(self, option):
        """Based on option return slow or fast time delay using sleep."""
        delay = random.randint(0, 2) if option == 1 else random.randint(0, 4)
        return delay

    def search_based_on_name(self, client_connection):
        self.lock.acquire()
        time.sleep(self.get_time_delay(1))
        reply = 'Enter the name'.encode('utf-8')
        client_connection.sendall(reply)
        ip_name = client_connection.recv(1024).decode('utf-8')
        found_ip = self.ip_dictionary[ip_name].encode('utf-8')
        client_connection.sendall(found_ip)
        self.lock.release()

    def search_based_on_ip(self, client_connection):
        self.lock.acquire()
        time.sleep(self.get_time_delay(1))
        found_ip = None
        reply = 'Enter the ip'.encode('utf-8')
        client_connection.sendall(reply)
        ip = client_connection.recv(1024).decode('utf-8')

        for key, value in self.ip_dictionary.items():
            if value == ip:
                found_ip = key.encode('utf-8')

        if found_ip is not None:
            client_connection.sendall(found_ip)
        else:
            client_connection.sendall('IP not found'.encode('utf-8'))
        self.lock.release()

    def insert_ip(self, client_connection):
        self.lock.acquire()
        time.sleep(self.get_time_delay(2))
        reply = 'enter the ip'.encode('utf-8')
        client_connection.sendall(reply)
        ip = client_connection.recv(1024).decode('utf-8')
        reply = 'enter the name of the ip'.encode('utf-8')
        client_connection.sendall(reply)
        ip_name = client_connection.recv(1024).decode('utf-8')

        new_entry = {str(ip_name): str(ip)}
        self.ip_dictionary.update(new_entry)

        reply = 'ip added'.encode('utf-8')
        client_connection.sendall(reply)
        self.lock.release()

    def delete_ip(self, client_connection):
        self.lock.acquire()
        time.sleep(self.get_time_delay(2))
        reply = 'enter the name of the ip to delete'.encode('utf-8')
        client_connection.sendall(reply)
        ip_to_delete = client_connection.recv(1024).decode('utf-8')

        del self.ip_dictionary[ip_to_delete]
        reply = 'ip succesfully deleted'.encode('utf-8')
        client_connection.sendall(reply)
        self.lock.acquire()

    def update_dictionary(self, client_connection):
        self.lock.acquire()
        time.sleep(self.get_time_delay(2))
        ips_dict_bytes = json.dumps(self.ip_dictionary,
                                    indent=4).encode('utf-8')

        client_connection.sendall(ips_dict_bytes)
        self.lock.release()

    def method_to_call(self, client_connection):
        client_choice = int(client_connection.recv(1024).decode('utf-8'))
        if client_choice == 1:
            self.search_based_on_name(client_connection)
        elif client_choice == 2:
            self.search_based_on_ip(client_connection)
        elif client_choice == 3:
            self.insert_ip(client_connection)
        elif client_choice == 4:
            self.delete_ip(client_connection)
        elif client_choice == 5:
            self.update_dictionary(client_connection)
        else:
            error_message = 'Error, select number from 1 to 5'.encode('utf-8')
            client_connection.sendall(error_message)

    def start_listening(self, connect):
        while True:
            self.method_to_call(connect)
        connect.close()

    def init(self):
        host = 'localhost'
        port = 6700
        connection_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        connection_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        connection_socket.bind((host, port))
        connection_socket.listen(5)

        while True:
            connect, adresss = connection_socket.accept()
            print('Socket created for {}'.format(adresss))
            threading.Thread(self.start_listening, (connect, )).start()

        connection_socket.close()

if __name__ == '__main__':
    Server().start_listening()
